import { Form, useLoaderData } from "@remix-run/react";
import { db } from "../db.server";
import { ActionFunctionArgs, redirect } from "@remix-run/node";
import { Prisma } from "@prisma/client";

let user: Prisma.UserCreateManyInput;

export const loader = async () => {
  const company = await db.company.findMany({
    select: {
      id: true,
      name: true,
    },
  });
  return company;
};

export const action = async ({ request }: ActionFunctionArgs) => {
  const formData = await request.formData();
  const objectData = Object.fromEntries(formData);

  user = {
    name: objectData.name.toString(),
    username: objectData.username.toString(),
    email: objectData.email.toString(),
    phone: objectData.phone.toString(),
    website: objectData.website.toString(),
    address: JSON.stringify({
      street: objectData.name,
      suite: objectData.suite,
      city: objectData.city,
      zipcode: objectData.zipcode,
    }),
    companyId: parseInt(objectData.company.toString()),
  };

  await db.user.create({
    data: user,
  });

  return redirect(`/`);
};

export default function ProfileEdit() {
  const company = useLoaderData<typeof loader>();

  return (
    <div className="flex flex-col bg-[#31313A] w-full p-4 md:p-8 shadow-lg text-amber-500 gap-4">
      <div className="text-xl font-bold">Add New People</div>
      <Form id="profile-form" method="post" className="flex flex-col gap-4">
        <div className="flex flex-col md:grid md:grid-cols-2 gap-4 text-stone-800">
          <div className="flex flex-col gap-1">
            <div className="text-stone-300 text-lg font-normal">Name</div>
            <input
              type="text"
              name="name"
              id="name"
              className="px-3 py-1"
              placeholder="Full Name"
              required
            />
          </div>
          <div className="flex flex-col gap-1">
            <div className="text-stone-300 text-lg font-normal">Username</div>
            <input
              type="text"
              name="username"
              id="username"
              className="px-3 py-1"
              placeholder="Unique Username"
              required
            />
          </div>
          <div className="flex flex-col gap-1">
            <div className="text-stone-300 text-lg font-normal">Email</div>
            <input
              type="text"
              name="email"
              id="email"
              className="px-3 py-1"
              placeholder="email@email.com"
              required
            />
          </div>
          <div className="flex flex-col gap-1">
            <div className="text-stone-300 text-lg font-normal">Phone</div>
            <input
              type="text"
              name="phone"
              id="phone"
              className="px-3 py-1"
              placeholder="+6203265489654"
              required
            />
          </div>
          <div className="flex flex-col gap-1 col-span-2">
            <div className="text-stone-300 text-lg font-normal">Address</div>
            <div className="grid grid-cols-2 gap-x-4 gap-y-2 w-full">
              <input
                type="text"
                name="street"
                id="street"
                className="px-3 w-full py-1"
                placeholder="Street number"
                required
              />
              <input
                type="text"
                name="suite"
                id="suite"
                className="px-3 w-full py-1"
                placeholder="Suite name"
                required
              />
              <input
                type="text"
                name="city"
                id="city"
                className="px-3 w-full py-1"
                placeholder="City name"
                required
              />
              <input
                type="text"
                name="zipcode"
                id="zipcode"
                className="px-3 w-full py-1"
                placeholder="Zipcode number"
                required
              />
            </div>
          </div>
          <div className="flex flex-col gap-1">
            <div className="text-stone-300 text-lg font-normal">Website</div>
            <input
              type="text"
              name="website"
              id="website"
              className="px-3 py-1"
              placeholder="www.site.com"
              required
            />
          </div>
          <div className="flex flex-col gap-1">
            <div className="text-stone-300 text-lg font-normal">Name</div>
            {/* <input
            type="text"
            name="name"
            id="name"
            className="px-3 py-1"
            placeholder="Full Name"
            required
          /> */}
            <select name="company" id="company" className="px-3 py-1">
              {company &&
                company.map((c) => {
                  return (
                    <option key={c.id} value={c.id}>
                      {c.name}
                    </option>
                  );
                })}
            </select>
          </div>
        </div>
        <button
          type="submit"
          className="bg-amber-500 text-white py-1 px-3 text-lg font-bold w-full"
        >
          Save
        </button>
      </Form>
    </div>
  );
}
