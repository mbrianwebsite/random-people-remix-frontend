/* eslint-disable  @typescript-eslint/no-explicit-any */

import type { LoaderFunctionArgs, MetaFunction } from "@remix-run/node";
import { Form, Link, redirect, useLoaderData } from "@remix-run/react";
import { db } from "../db.server";

export const meta: MetaFunction<typeof loader> = ({ data }) => {
  if (!data || !data.q) {
    return [
      { title: "Random People" },
      { name: "description", content: "Random people on internet." },
    ];
  } else if (data.profiles.length == 0) {
    return [
      { title: 'Search for "' + data.q + '" not Found | Random People' },
      { name: "description", content: "Random people on internet." },
    ];
  } else {
    return [
      { title: 'Search for "' + data.q + '" | Random People' },
      { name: "description", content: "Random people on internet." },
    ];
  }
};

export const loader = async ({ request }: LoaderFunctionArgs) => {
  let whereQuery: any = {};

  const url = new URL(request.url);
  const q = url.searchParams.get("q");

  if (q) {
    const name = {
      name: {
        startsWith: "%" + q + "%",
      },
    };
    whereQuery = name;
  }

  const profiles = await db.user.findMany({
    select: {
      id: true,
      name: true,
      address: true,
      company: true,
      email: true,
      phone: true,
      username: true,
      website: true,
    },
    where: whereQuery,
  });

  if (!profiles) {
    return console.log("Fetching Profile Error");
  }

  return { profiles, q };
};

export const action = async () => {
  return redirect(`/profile/new`);
};

export default function Index() {
  const { profiles } = useLoaderData<typeof loader>();
  return (
    <div className="flex flex-col gap-8">
      <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 gap-8 items-center bg-[#31313A] justify-between w-full p-4 md:p-8 shadow-lg text-amber-500">
        {profiles &&
          profiles.map((p: any, index: any) => {
            return (
              <Link
                to={"/profile/" + p.id}
                key={index}
                className="flex flex-col p-4 gap-4 border border-amber-500 break-words h-full hover:shadow-lg hover:shadow-amber-400"
              >
                <img
                  src={"https://robohash.org/" + p.name + ".png"}
                  alt="profile"
                />
                <div className="flex flex-col">
                  <div className="font-bold text-xl">{p.name}</div>
                  <div className="font-medium text-stone-300">{p.email}</div>
                  <div className="text-stone-400">{p.address.city}</div>
                  <div className="text-stone-500">{p.phone}</div>
                </div>
              </Link>
            );
          })}
        <div className="flex flex-col items-center justify-center p-4 gap-4 border border-amber-500 break-words h-full hover:shadow-lg hover:shadow-amber-400">
          <Form method="post">
            <button
              type="submit"
              className="bg-amber-500 text-white py-1 px-3 text-xl "
            >
              Add New
            </button>
          </Form>
        </div>
      </div>
    </div>
  );
}
