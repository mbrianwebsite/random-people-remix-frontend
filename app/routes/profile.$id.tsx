import {
  ActionFunctionArgs,
  LoaderFunctionArgs,
  MetaFunction,
  redirect,
} from "@remix-run/node";
import { Form, Link, useLoaderData } from "@remix-run/react";
import { db } from "../db.server";
import React from "react";

export const loader = async ({ params }: LoaderFunctionArgs) => {
  if (!params.id) return console.log("Fetching Profile Error");

  const profile = await db.user.findMany({
    where: { id: parseInt(params.id) },
    select: {
      id: true,
      name: true,
      address: true,
      company: true,
      email: true,
      phone: true,
      username: true,
      website: true,
    },
  });

  if (!profile) {
    return console.log("Fetching Profile Error");
  }

  return profile;
};

export const meta: MetaFunction<typeof loader> = ({ data }) => {
  if (!data) {
    return [
      { title: "Random People" },
      { name: "description", content: "Random people on internet." },
    ];
  }
  return [
    { title: data[0].name + " | " + "Random People" },
    { name: "description", content: "Random people on internet." },
  ];
};

export const action = async ({ request }: ActionFunctionArgs) => {
  const formData = await request.formData();
  const objectData = Object.fromEntries(formData);

  await db.user.delete({
    where: { id: parseInt(objectData.id.toString()) },
  });

  return redirect("/");
};

export default function Profile() {
  const [deleteModal, setDeleteModal] = React.useState(false);

  const profile = useLoaderData<typeof loader>();
  const p = profile[0];

  return (
    <div className="relative">
      <div className="flex flex-col md:flex-row">
        {profile && (
          <>
            <div className="flex flex-row gap-8 items-center bg-[#31313A] justify-between w-fit p-4 md:p-8 shadow-lg shadow-black text-amber-500 z-10">
              <div className="flex flex-col  gap-4 border border-amber-500 break-words h-full hover:shadow-lg hover:shadow-amber-400 ">
                <img
                  src={"https://robohash.org/" + p.name + ".png"}
                  alt="profile"
                />
              </div>
            </div>
            <div className="flex flex-row my-4 md:-ml-4 gap-8 bg-[#31313A] justify-between md:w-fit p-4 md:p-8 shadow-lg shadow-black text-amber-500">
              <div className="flex flex-col w-full pl-4 md:pl-6 gap-4">
                <div className="flex flex-col">
                  <div className="flex flex-col">
                    <div className="flex flex-row justify-between">
                      <div className="font-bold text-xl">{p.name}</div>
                      <div className="flex flex-row gap-1">
                        <Link to={"/profile/edit/" + p.id}>
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            fill="none"
                            viewBox="0 0 24 24"
                            strokeWidth="1.5"
                            stroke="currentColor"
                            className="w-6 h-6"
                          >
                            <path
                              strokeLinecap="round"
                              strokeLinejoin="round"
                              d="m16.862 4.487 1.687-1.688a1.875 1.875 0 1 1 2.652 2.652L10.582 16.07a4.5 4.5 0 0 1-1.897 1.13L6 18l.8-2.685a4.5 4.5 0 0 1 1.13-1.897l8.932-8.931Zm0 0L19.5 7.125M18 14v4.75A2.25 2.25 0 0 1 15.75 21H5.25A2.25 2.25 0 0 1 3 18.75V8.25A2.25 2.25 0 0 1 5.25 6H10"
                            />
                          </svg>
                        </Link>

                        <svg
                          onClick={() => setDeleteModal(true)}
                          xmlns="http://www.w3.org/2000/svg"
                          fill="none"
                          viewBox="0 0 24 24"
                          strokeWidth="1.5"
                          stroke="currentColor"
                          className="w-6 h-6"
                        >
                          <path
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            d="m14.74 9-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 0 1-2.244 2.077H8.084a2.25 2.25 0 0 1-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 0 0-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 0 1 3.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 0 0-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 0 0-7.5 0"
                          />
                        </svg>

                        {/* <Form method="POST">
                        <button type="submit"></button>
                      </Form> */}
                      </div>
                    </div>
                    <div className="font-medium text-stone-300">{p.email}</div>
                    <div className="text-stone-400">
                      {JSON.parse(p.address).city}
                    </div>
                    <div className="text-stone-500">{p.phone}</div>
                  </div>
                </div>
                <div className="flex flex-col">
                  <div className="font-bold text-lg">{p.company.name}</div>
                  <div className="text-stone-300">{p.company.catchPhrase}</div>
                  <div className="text-stone-400">{p.company.bs}</div>
                  <div className="text-stone-500">
                    {JSON.parse(p.address).city},{" "}
                    {JSON.parse(p.address).zipcode}
                  </div>
                  <div className="text-stone-500"></div>
                  <div className="text-stone-500">{p.website}</div>
                </div>
              </div>
            </div>
          </>
        )}
      </div>
      {deleteModal && (
        <div className="absolute top-0 left-0 z-10 w-full h-full flex flex-col items-center justify-center bg-black bg-opacity-75 gap-2">
          <div className="text-white text-lg font-bold text-center ">
            Are you sure you want to delete
            <br />
            {p.name} profile?
          </div>
          <div className="flex flex-row gap-3">
            <Form method="post">
              <input type="hidden" value={p.id} name="id" />
              <button
                type="submit"
                className="bg-amber-500 text-white py-1 px-3 text-md font-bold min-w-24"
              >
                Yes
              </button>
            </Form>
            <button
              className="bg-amber-500 text-white py-1 px-3 text-md font-bold min-w-24"
              onClick={() => setDeleteModal(false)}
            >
              Cancel
            </button>
          </div>
        </div>
      )}
    </div>
  );
}
